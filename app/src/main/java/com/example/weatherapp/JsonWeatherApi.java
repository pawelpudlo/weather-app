package com.example.weatherapp;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface JsonWeatherApi {

    @GET("data/2.5/weather")
    Call<Post> getCurrentWeatherData(
            @Query("q") String city,
            @Query("APPID") String app_id,
            @Query("units") String metric
    );
}
