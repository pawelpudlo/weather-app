package com.example.weatherapp;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WeatherActivity extends AppCompatActivity {

    private TextView tempDisp;
    private TextView cityName;
    private TextView clock;
    private TextView pressDisp;
    private TextView humidityDisp;
    private TextView minDisp;
    private TextView maxDisp;
    private int status=1;
    private ImageView image;

    NetworkInfo networkInfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.status=1;
        ConnectivityManager connectivityManager = (ConnectivityManager)
                getSystemService(this.CONNECTIVITY_SERVICE);

        this.networkInfo = connectivityManager.getActiveNetworkInfo();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        image= findViewById(R.id.weatherIcon);

        final Intent intent = new Intent(this, MainActivity.class);

            Thread myThread = null;
            Runnable run = new ClockRun();
            myThread= new Thread(run);
            myThread.start();

        final SwipeRefreshLayout pullToRefresh = findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(networkInfo != null && networkInfo.isConnected()){
                    pullToRefresh.setRefreshing(false);
                }else{
                    pullToRefresh.setRefreshing(false);
                    startActivity(intent);
                }
            }
        });

    }

    String link = "";

    public void getWeather(){

        final Intent intent = new Intent(this, MainActivity.class);
        if(networkInfo != null && networkInfo.isConnected()){
            String cityW = getIntent().getStringExtra("CityName");
            final String cw = cityW;

            Retrofit retfrofit = new Retrofit.Builder()
                    .baseUrl("https://api.openweathermap.org/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            cityW += ",pl";




            JsonWeatherApi ser = retfrofit.create(JsonWeatherApi.class);
            Call<Post> call = ser.getCurrentWeatherData(cityW, "749561a315b14523a8f5f1ef95e45864", "metric");
            call.enqueue(new Callback<Post>() {
                @Override
                public void onResponse(Call<Post> call, Response<Post> response) {

                    Post weatherResponse = response.body();
                    double temp = weatherResponse.getMain().getTemp();
                    int press = weatherResponse.getMain().getPressure();
                    double humidity = weatherResponse.getMain().getHumidity();
                    double min = weatherResponse.getMain().getTemp_min();
                    double max = weatherResponse.getMain().getTemp_max();

                    cityName = findViewById(R.id.cityName);
                    tempDisp = findViewById(R.id.tempDisplay);
                    clock = findViewById(R.id.Clock);
                    pressDisp = findViewById(R.id.pressureDisplay);
                    humidityDisp = findViewById(R.id.humidityDisplay);
                    minDisp = findViewById(R.id.minDisplay);
                    maxDisp = findViewById(R.id.maxDisplay);
                    image = findViewById(R.id.weatherIcon);

                    link = link +  "https://openweathermap.org/img/wn/" +  weatherResponse.weather.get(0).icon +".png";
                   // Picasso.with(this).load(link).into(image);
                    getTime();
                    tempDisp.setText(temp+"\u2103");
                    pressDisp.setText(press+" hPa");
                    humidityDisp.setText(humidity+"%");
                    minDisp.setText(min+"\u2103");
                    maxDisp.setText(max+"\u2103");
                    cityName.setText(cw);

                    Picasso.get().load(link).into(image);


                }

                @Override
                public void onFailure(Call<Post> call, Throwable t) {
                }
            });

        }else{
            if(this.status==1){
                this.status=0;
                startActivity(intent);
            }
        }


    }

    public void getTime(){
        Date today = new Date();

        @SuppressLint("SimpleDateFormat") DateFormat df = new SimpleDateFormat("HH:mm");

        df.setTimeZone(java.util.TimeZone.getDefault());
        String localT = df.format(today);
        clock.setText(localT);
    }

    class ClockRun implements Runnable{
        // @Override
        ConnectivityManager connectivityManager = (ConnectivityManager)
                getSystemService(CONNECTIVITY_SERVICE);
        public void run() {

            while(!Thread.currentThread().isInterrupted()){
                try {

                    getWeather();

                    networkInfo = connectivityManager.getActiveNetworkInfo();
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }catch(Exception e){
                }
            }
        }
    }

}
