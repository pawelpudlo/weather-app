package com.example.weatherapp;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    EditText editText;

    public void saveData(String output){
        SharedPreferences sharedPreferences = getSharedPreferences("TEST",MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("DATA_KEY",output);
        editor.apply();
    }
    private void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences("TEST",MODE_PRIVATE);
        String date = sharedPreferences.getString("DATA_KEY","");
        editText.setText(date);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = findViewById(R.id.textView);
        refresh();
        loadData();
    }

    private void refreshRun(int milliseconds){

        final Handler handler = new Handler();

        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                refresh();
            }
        };
        handler.postDelayed(runnable,milliseconds);
    }


    public void refresh(){
        TextView tv = findViewById(R.id.connectViev);
        Button button = findViewById(R.id.button);

        ConnectivityManager connectivityManager = (ConnectivityManager)
                getSystemService(this.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()){
            tv.setVisibility(View.INVISIBLE);
            button.setEnabled(true);
        }else{
            tv.setVisibility(View.VISIBLE);
            button.setEnabled(false);
        }
        refreshRun(1000);
    }

    public void CheckWeather(View view) {

        EditText et = findViewById(R.id.textView);

        final Intent intent = new Intent(this, WeatherActivity.class);
        String cityW = et.getText().toString();
        final String tmp = cityW;

        intent.putExtra("CityName",cityW);

        cityW.replaceAll("ą", "a");
        cityW.replaceAll("ę", "e");
        cityW.replaceAll("ć", "c");
        cityW.replaceAll("ś", "s");
        cityW.replaceAll("ń", "n");
        cityW.replaceAll("ó", "o");

        final String c = cityW;

        Retrofit retfrofit = new Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        cityW += ",pl";


        JsonWeatherApi ser = retfrofit.create(JsonWeatherApi.class);
        Call<Post> call = ser.getCurrentWeatherData(cityW, "749561a315b14523a8f5f1ef95e45864", "metric");
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(MainActivity.this, "Zla nazwa miasta", Toast.LENGTH_SHORT).show();
                    return;
                }
                intent.putExtra("CityName",c);
                saveData(tmp);
                startActivity(intent);
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Connection Error", Toast.LENGTH_SHORT).show();
            }
        });
    }
}

